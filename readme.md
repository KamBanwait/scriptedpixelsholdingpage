# Scripted Pixels Holding Page
Simple HTML holding page

<br />

## Netlify Status

Deployed with Netlify:

[![Netlify Status](https://api.netlify.com/api/v1/badges/c0c41b8f-1cfe-4b5a-ba52-9f39f9eaba34/deploy-status)](https://app.netlify.com/sites/scriptedpixels/deploys)
